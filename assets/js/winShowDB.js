let $ = require('jquery')

const Entities = require('html-entities').AllHtmlEntities
const entities = new Entities()
const humanFormat = require('human-format')
const log = require('electron-log')

const ipcRenderer = require('electron').ipcRenderer
const remote = require('electron').remote

let DB = null

function getTypes (content) {
  return content.map(function (value, idx, arr) {
    return value.type
      .replace('text/plain', 'text')
      .replace('text/html', 'html')
      .replace('text/rtf', 'rtf')
      .replace(';base64', '')
  })
}

function getRecSize (record) {
  return record.reduce(function (prev, curr, curridx, arr) {
    return prev + curr.content.length
  }, 0)
}

function displayDB () {
  const i18n = remote.getGlobal('i18next')
  const top_tmpl = ' \
    <div class="row mb-2"> \
      <div class="col-3 text-left"> \
        <a href="#" onclick="clickCopy(#IDX#)"> \
            <img src="./../images/icons/copy32.png" title="' + i18n.t('CopyToClipboard') + '" width="16" height="16"> \
        </a> \
        <a href="#" onclick="clickPop(#IDX#)"> \
            <img src="./../images/icons/pop32.png" title="' + i18n.t("PopToClipboard") + '" width="16" height="16"> \
        </a> \
        <a href="#" onclick="clickRemove(#IDX#)"> \
            <img src="./../images/icons/trash32.png" title="' + i18n.t("Remove") + '" width="16" height="16"> \
        </a> \
      </div>\
      <div class="col-9 text-right"> \
          <span class="text-muted"><small>#TYPES# <br>#SIZE# - #TS#</small></div><span> \
      </div> \
    </div> \
  '
  const content_tmpl = ' \
    <div class="row"> \
      <div class="col"> \
        #CONTENT# \
      </div> \
    </div> \
  '
  const bottom_tmpl = ' \
    <hr> \
  '
  let cbsize = 0
  $('#cb-content').text('')
  for (let i = 0; i < DB.records.length; i++) {
    let cbpack = ''
    let rec = DB.records[i]
    let contents = rec.content
    let recsize = getRecSize(contents)
    cbsize += recsize
    let type = rec.type
    let timestamp = new Date(rec.timestamp)
    if (type === 'multi-value') {
      let types = getTypes(contents)
      cbpack = top_tmpl
        .replace('#TYPES#', types)
        .replace('#SIZE#', humanFormat(recsize, { decimals: 2 }))
        .replace('#TS#', new Date(timestamp).toLocaleString())
        .replace(/#IDX#/g, i)
      for (let j = 0; j < contents.length; j++) {
        let content = contents[j]
        if ((content.type === 'text/html') && (types.length === 1)) {
          let encoded = entities.encodeNonUTF(content.content)
          cbpack = cbpack + '<pre>' + content_tmpl.replace('#CONTENT#', encoded) + '</pre>'
        }
        if ((content.type === 'text/rtf') && (types.length === 1)) {
          let encoded = entities.encodeNonUTF(content.content)
          cbpack = cbpack + content_tmpl.replace('#CONTENT#', encoded)
        }
        if ((content.type === 'bookmark') && (types.length === 1)) {
          let bookmark = content.content.title + '<br>' + content.content.url
          let encoded = entities.encodeNonUTF(bookmark)
          cbpack = cbpack + content_tmpl.replace('#CONTENT#', encoded)
        }
        if (content.type === 'text/plain') {
          let encoded = '<pre>' + entities.encodeNonUTF(content.content) + '</pre>'
          cbpack = cbpack + content_tmpl.replace('#CONTENT#', encoded)
        }
        if (content.type.substr(0, 5) === 'image') {
          let html = "<img style='display:block; height:128px;' src='data:" + content.type + ', ' + content.content + "'/>"
          cbpack = cbpack + html
        }
      }
      cbpack = cbpack + bottom_tmpl
      $('#cb-content').append(cbpack)
    }
  }
  $('#dbcount').text(DB.records.length)
  $('#dbsize').text(humanFormat(cbsize), { decimals: 2 })
  $('#countlabel').text(i18n.t('Clips'))
  $('#sizelabel').text(i18n.t('Symbols'))
  $('#removeAllImg').attr('title', i18n.t('ClearHistory'))
  $(document).attr('title', i18n.t('showDBWinTitle'))
}

function clickRemoveAll () {
  ipcRenderer.send('request-removeAll')
  log.debug('request removing all items')
}

function clickRemove (idx) {
  ipcRenderer.send('request-remove', idx)
  log.debug('request item removing #' + idx)
}

function clickCopy (idx) {
  ipcRenderer.send('request-copy', idx)
  log.debug('request copy item #' + idx)
}

function clickPop (idx) {
  clickCopy(idx)
  clickRemove(idx)
  log.debug('request popitem #' + idx)
}

function updateDB () {
  DB = ipcRenderer.sendSync('request-data')
  displayDB()
}

ipcRenderer.on('detailedview-update', function (event, arg) {
  updateDB()
})

$(document).ready(function () {
  updateDB()
})

//   _____ _ _       _                         _ __  __
//  / ____| (_)     | |                       | |  \/  |
// | |    | |_ _ __ | |__   ___   __ _ _ __ __| | \  / | ___ _ __ ___   ___
// | |    | | | '_ \| '_ \ / _ \ / _` | '__/ _` | |\/| |/ _ \ '_ ` _ \ / _ \
// | |____| | | |_) | |_) | (_) | (_| | | | (_| | |  | |  __/ | | | | | (_) |
//  \_____|_|_| .__/|_.__/ \___/ \__,_|_|  \__,_|_|  |_|\___|_| |_| |_|\___/
//            | |
//            |_|

const electron = require('electron')
const ipcMain = electron.ipcMain
const BrowserWindow = electron.BrowserWindow
const globalShortcut = electron.globalShortcut
const Notification = electron.Notification
const Tray = electron.Tray

const openAboutWindow = require('about-window').default
const ElectronStore = require('electron-store')
const log = require('electron-log')
const i18next = require('i18next')
const I18nBackend = require('i18next-node-fs-backend')
const Tock = require('tocktimer')

const config = require('./assets/js/config/app.js')
const ClipboardManagerSingleton = require('./assets/js/clipboardmanager.js')
const DatabaseSingleton = require('./assets/js/database/database.js')
const MenuFactorySingleton = require('./assets/js/menufactory.js')

const app = electron.app
const prefs = new ElectronStore(config.get('app.preferences.store'))
const clipboardManager = new ClipboardManagerSingleton.getInstance()
let database = new DatabaseSingleton.getInstance()
let menuFactory = new MenuFactorySingleton.getInstance()

let MODEPAUSE = false
let CONTEXTMENU = null
let TRAY = null
let WINSHOWCLIP = null

// -------------------------------------------------------------------------

function clickLng (language) {
  prefs.set('lng', language)
  i18next.changeLanguage(language, updateMenuItems)
}

function clearDB () {
  database.clear()
}

function db2Clipboard (idx) {
  let contents = database.get(idx).content
  clipboardManager.setClipboard(contents)
  log.info('< ' + contents[0].type + '...')
  notify('ClipBoardMemo', 'Now in clipboard :', contents[0].content)
}

function notify (title, subtitle, body) {
  if (Notification.isSupported()) {
    if (prefs.get('Notifications', false) === true) {
      let notif = new Notification({
        title: title,
        subtitle: subtitle,
        body: contentextract(body),
        silent: true,
        icon: config.get('app.iconPath')
      })
      notif.show()
    }
  }
}
global.notify = notify

function contentextract (content) {
  let lblsuite = ''
  if (content.length > config.get('app.dock.items.labelMaxChars')) {
    lblsuite = '...'
  }
  return content.substr(0, config.get('app.dock.items.labelMaxChars')) + lblsuite
}

// -------------------------------------------------------------------------

function clickClearDB () {
  clearDB()
  updateMenuItems()
  log.info('> (clear)')
}

function mnuClickManager (mnuitem, broWindow, event) {
  switch (mnuitem.id) {
    case 'about':
      openAboutWindow({
        icon_path: config.get('app.aboutWindows.icon_path'),
        copyright: config.get('app.aboutWindows.copyright'),
        package_json_dir: config.get('app.aboutWindows.package_json_dir'),
        homepage: config.get('app.aboutWindows.homepage'),
        bug_report_url: config.get('app.aboutWindows.bug_report_url'),
        use_version_info: config.get('app.aboutWindows.use_version_info')
      })
      break
    case 'clear':
      clickClearDB()
      break
    case 'lng_en':
      clickLng('en')
      break
    case 'lng_fr':
      clickLng('fr')
      break
    case 'lng_es':
      clickLng('es')
      break
    case 'notifications':
      let minotif = CONTEXTMENU.getMenuItemById('notifications')
      prefs.set('Notifications', minotif.checked)
      updateStartStatus()
      break
    case 'pack':
      let cbpack = ''
      for (let i = database.count() - 1; i >= 0; i--) {
        let rec = database.get(i)
        if (rec.type === 'multi-value') {
          for (let j = 0; j < rec.content.length; j++) {
            if (rec.content[j].type === 'text/plain') {
              cbpack += rec.content[j].content + '\n'
            }
          }
        }
      }
      if (cbpack !== '') {
        database.addRecord([
          { content: cbpack, type: 'text/plain' }
        ], 'multi-value')
        db2Clipboard(0)
        updateMenuItems()
        log.info('> (pack)')
      }
      break
    case 'pause':
      MODEPAUSE = !MODEPAUSE
      log.info('Pause: ' + MODEPAUSE)
      CONTEXTMENU.getMenuItemById('pause').checked = MODEPAUSE
      if (!MODEPAUSE) clipboardManager.init()
      break
    case 'pop':
      log.info('CommandOrControl+Alt+V is pressed')
      if (database.count() > 0) {
        db2Clipboard(0)
        database.delRecord(0)
        updateMenuItems()
      }
      break
    case 'showDB':
      createWindowShowClipboard()
      break
    case 'startatlogin':
      let misal = CONTEXTMENU.getMenuItemById('startatlogin')
      prefs.set('StartAtLogin', misal.checked)
      updateStartStatus()
      break
    default:
      log.warn('no function defined for "' + mnuitem.id + '" menu option !')
      db2Clipboard(mnuitem.id)
  }
}

ipcMain.on('request-copy', function (event, arg) {
  db2Clipboard(arg)
})

ipcMain.on('request-data', function (event, arg) {
  event.returnValue = database
})

ipcMain.on('request-remove', function (event, arg) {
  database.delRecord(arg)
  updateMenuItems()
})

ipcMain.on('request-removeAll', function (event, arg) {
  clickClearDB()
  event.returnValue = database
})

// -------------------------------------------------------------------------

function updateDetailedView () {
  if (WINSHOWCLIP !== null) {
    WINSHOWCLIP.webContents.send('detailedview-update')
  }
}

function updateMenuItems () {
  CONTEXTMENU = menuFactory.update(database, MODEPAUSE, mnuClickManager)
  TRAY.setContextMenu(CONTEXTMENU)
  updateDetailedView()
}

function updateStartStatus () {
  let misal = prefs.get('StartAtLogin', true)
  let minotif = prefs.get('Notifications', false)
  app.setLoginItemSettings({
    openAtLogin: misal,
    openAsHidden: minotif
  })
  if (misal) {
    log.info('StartAtLogin checked')
  } else {
    log.info('StartAtLogin unchecked')
  }
  if (minotif) {
    log.info('Notifications checked')
  } else {
    log.info('Notifications unchecked')
  }
  prefs.set('StartAtLogin', misal)
  prefs.set('Notifications', minotif)
}

// ----------------------------------------------------------------------------

function launchGetClipboard () {
  clipboardManager.init()
  var timer = new Tock({
    countdown: false,
    interval: 500,
    callback: function () {
      let cbcontent = clipboardManager.getFromClipboard()
      if ((cbcontent.new) && (!MODEPAUSE)) {
        if (!clipboardManager.ignorenext) {
          if (cbcontent.content.length > 0) {
            log.debug('adding to database...')
            database.addRecord(
              clipboardManager.encode64(cbcontent.content),
              'multi-value'
            )
            updateMenuItems()
          }
        } else {
          clipboardManager.ignorenext = false
        }
      }
    }
  })

  timer.start()
}

// ----------------------------------------------------------------------------

function createWindowShowClipboard () {
  if (WINSHOWCLIP === null) {
    WINSHOWCLIP = new BrowserWindow({
      width: 400,
      height: 400,
      minwidth: 400,
      minheight: 200,
      frame: true,
      maximized: false,
      movable: true,
      resizable: true,
      show: false,
      fullscreen: false,
      minimizable: false,
      opacity: 0.95
    })

    if (process.env.NODE_ENV === 'development') {
      WINSHOWCLIP.webContents.openDevTools()
    }

    const position = getWindowPosition()
    WINSHOWCLIP.setPosition(position.x, position.y, false)
    WINSHOWCLIP.loadURL(config.get('app.historicWindow.url'))

    WINSHOWCLIP.on('closed', function () {
      WINSHOWCLIP = null
    })
    WINSHOWCLIP.show()
    WINSHOWCLIP.focus()
  } else if (BrowserWindow.getFocusedWindow() !== WINSHOWCLIP) {
    WINSHOWCLIP.focus()
  } else {
    WINSHOWCLIP.hide()
    if (process.env.NODE_ENV === 'development') {
      WINSHOWCLIP.webContents.closeDevTools()
    }
    WINSHOWCLIP = null
  }
}

function getWindowPosition () {
  const windowBounds = WINSHOWCLIP.getBounds()
  let x = 0
  let y = 0
  if (process.platform === 'darwin') {
    const trayBounds = TRAY.getBounds()
    x = Math.round(trayBounds.x + (trayBounds.width / 2) - (windowBounds.width / 2))
    y = Math.round(trayBounds.y + trayBounds.height + 3)
  } else {
    const Screen = electron.screen
    const screenSize = Screen.getDisplayNearestPoint(Screen.getCursorScreenPoint()).workArea
    x = Math.floor(screenSize.x + ((screenSize.width / 2) - (windowBounds.width / 2)))
    y = Math.floor(((screenSize.height + screenSize.y) / 2) - (windowBounds.height / 2))
  }
  return { x: x, y: y }
}

function createTray () {
  TRAY = new Tray(config.get('app.iconPath'))
  TRAY.setToolTip('ClipBoardMemo')
}

// -------------------------------------------------------------------------

function initApp () {
  log.info('Loading configuration...')
  log.transports.file.level = process.env.NODE_ENV === 'development' ? 'debug' : 'debug'
  log.transports.console.level = process.env.NODE_ENV === 'development' ? 'debug' : 'info'
  log.transports.file.format = '{y}-{m}-{d} {h}:{i}:{s}:{ms} {level} {text}'
  log.transports.console.format = '{h}:{i}:{s}:{ms} {level} {text}'
  log.transports.file.maxSize = 5 * 1024 * 1024
  log.info('log file:' + log.transports.file.findLogPath())

  database.init(config.get('database'))
  log.info('database: ' + database.config.path)
  log.info('preferences: ' + prefs.path)

  if (process.env.NODE_ENV !== 'development') {
    if (process.platform === 'darwin') app.dock.hide()
  }

  log.info('Registering key ' + config.get('app.shortcuts.pop') + '...')
  let ret = globalShortcut.register(config.get('app.shortcuts.pop'), function () {
    mnuClickManager(CONTEXTMENU.getMenuItemById('pop'), null, null)
  })
  if (!ret) {
    log.error('failed')
  }

  log.info('Registering key ' + config.get('app.shortcuts.historic') + '...')
  ret = globalShortcut.register(config.get('app.shortcuts.historic'), function () {
    mnuClickManager(CONTEXTMENU.getMenuItemById('showDB'), null, null)
  })
  if (!ret) {
    log.error('failed')
  }

  log.info('Launching clipboard capture...')
  launchGetClipboard()

  log.info('Initializing tray...')
  updateStartStatus()
  createTray()

  log.info('Loading i18n configuration...')
  let lng = prefs.get('lng', config.get('i18n').fallbackLng)
  if (config.get('i18n').whitelist.indexOf(app.getLocale()) > -1) {
    lng = prefs.get('lng', app.getLocale())
  }
  prefs.set('lng', lng)
  let i18nOptions = config.get('i18n')
  i18nOptions.lng = lng
  i18next
    .use(I18nBackend)
    .init(i18nOptions, function () {
      global.i18next = i18next
      menuFactory.init(config, prefs, i18next)
      updateMenuItems()
    })
}

// -------------------------------------------------------------------------

app.on('window-all-closed', function () {
  // if (process.platform !== 'darwin') {
  //   app.quit()
  // }
})

app.on('will-quit', function () {
  log.info('Unregistering ' + config.get('app.shortcuts.pop') + '...')
  globalShortcut.unregister(config.get('app.shortcuts.pop'))
  log.info('Unregistering ' + config.get('app.shortcuts.historic') + '...')
  globalShortcut.unregister(config.get('app.shortcuts.historic'))
})

app.on('ready', initApp)

const chai = require('chai')
const assert = chai.assert
const Record = require('../../assets/js/database/record')

describe('Record class tests', function () {
  let bts = new Date()
  let r = new Record('foo data', 'text/plain')
  let ats = new Date()

  it('Created record have type, content and timestamp attributes', function () {
    assert.equal(r.type, 'text/plain')
    assert.equal(r.content, 'foo data')
    assert.isAtLeast(bts, r.timestamp)
    assert.isAtMost(ats, r.timestamp)
  })

  it('Should return the length of the content', function () {
    assert.equal(8, r.length())
  })

  it('Should return the full size of the record', function () {
    assert.equal(42, r.size(), r.get())
  })

  it('Should return record as an object', function () {
    let result = r.get()
    assert.isObject(result)
    assert.equal(result.type, 'text/plain')
    assert.equal(result.content, 'foo data')
    assert.isAtLeast(bts, result.timestamp)
    assert.isAtMost(ats, result.timestamp)
  })

  it('Should set the record from an object', function () {
    let ts = new Date()
    r.set({
      type: 'text',
      content: 'bar foo',
      timestamp: ts
    })
    assert.equal('text', r.type)
    assert.equal('bar foo', r.content)
    assert.equal(ts, r.timestamp)
  })
})
